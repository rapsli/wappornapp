// help http://stackoverflow.com/questions/6050384/dynamically-create-jquery-mobile-page-via-javascript-after-clicking

var GRAPHML_FILE = 'appvsweb.graphml';
var ID_NODE_DESCRIPTION = 'd5'; // Attribute ID used for node description
var ID_EDGE_DESCRIPTION = 'd9'; // Attribute ID used for edge description

//$(document).bind('pageinit', function() {
$(document).ready(function(){
	// Check if "key" exists in the storage
	var node_id = $.jStorage.get("current_location");
	if (!node_id){
		$.jStorage.set("current_location", "n0");
	}
	fetchXML();
});

jQuery.fn.exists = function(){return this.length>0;}

function fetchXML() {
	$.ajax({
        type: "GET",
		url: GRAPHML_FILE,
		dataType: "xml",
		success: getCurrentNodeContent
	});
}

/**
	Take the info from the XML and make it into app form
*/
function getCurrentNodeContent(xml) {
	node_id = $.jStorage.get("current_location");
	
	if (!$('#'+node_id).exists()) { // only if the page doesn't exists yet
		var question = ''; 
		question += '<div data-role="header">		<h1>Web App or Native App?</h1>	</div>';
		
		question += '<div data-role="content" >';
		// get the description of the node -> this is the question
		question += $(xml).find('#'+node_id +'> data[key="'+ID_NODE_DESCRIPTION+'"]').text();
		
		$(xml).find('edge[source="'+node_id+'"] > data[key="'+ID_EDGE_DESCRIPTION+'"]').each(function(){
			question += createButton($(this).text(), $(this).parent().attr('target'));
		});
		
		// reset button
		question += createButton('restart', "n0");
		
		question += '</div>';
		// footer
		question += '<div data-role="footer"> <h4>Provided by <a href="http://rapsli.ch" target="_blank">rapsli.ch</a></h4>     </div>';
		
		//append the new page onto the end of the body
		$('#page_body').append('<div data-role="page" id="' + node_id + '">' + question + '</div>');
		
	}
		
	//initialize the new page 
    $.mobile.initializePage();

    //navigate to the new page
    $.mobile.changePage("#" + node_id, {transition: "slideup"});
}


function createButton(link, value) {
	src = '<a class="next-target" href="#"'+value+' data-role="button" data-target="'+value+'">'+link+'</a>';
	return src;
}

$('a.next-target').live('click', function(event){
	// update current location
	$.jStorage.set("current_location", $(this).attr('data-target'));
	//$('#content').empty();
	fetchXML();
//	return false;
});
