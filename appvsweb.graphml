<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xmlns:yed="http://www.yworks.com/xml/yed/3" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
  <!--Created by yFiles for Java 2.9-->
  <key for="graphml" id="d0" yfiles.type="resources"/>
  <key for="port" id="d1" yfiles.type="portgraphics"/>
  <key for="port" id="d2" yfiles.type="portgeometry"/>
  <key for="port" id="d3" yfiles.type="portuserdata"/>
  <key attr.name="url" attr.type="string" for="node" id="d4"/>
  <key attr.name="description" attr.type="string" for="node" id="d5"/>
  <key for="node" id="d6" yfiles.type="nodegraphics"/>
  <key attr.name="Beschreibung" attr.type="string" for="graph" id="d7"/>
  <key attr.name="url" attr.type="string" for="edge" id="d8"/>
  <key attr.name="description" attr.type="string" for="edge" id="d9"/>
  <key for="edge" id="d10" yfiles.type="edgegraphics"/>
  <graph edgedefault="directed" id="G">
    <data key="d7"/>
    <node id="n0">
      <data key="d5"><![CDATA[<p>This little guide should help you figure out what the best strategy for your online presence could be. Basically there are four options:</p>
<ul>
<li>No mobile presense</li>
<li>A mobile optimized website</li>
<li>A web app based on HTML5 technology</li>
<li>A native mobile app, e.g.: iApp or Android app</li>
</ul>
<p>Follow the little guide, answer the question and see what is best suited for you. This page is best viewed on a mobile device such as iPhone or Android.</p>

<p class="question">Do I need to consider mobile for your online presence?</p>]]></data>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="218.0" x="69.25" y="0.0"/>
          <y:Fill color="#FFCC00" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="13" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="19.125" modelName="custom" textColor="#000000" visible="true" width="198.30810546875" x="9.845947265625" y="5.4375">Do I need to consider mobile?<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
    <node id="n1">
      <data key="d4"/>
      <data key="d5"><![CDATA[Mostly there are two types of websites: 
<ol>
<li>Websites that are mostly content driven like corporate websites that just give information.</li>
<li>Websites that help a user accomplish a task, e.g. an e-commerce, a navigation tool and so on.</li>
</ol>
<p class="question">Is your online presence largely content driven?</p>
]]></data>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="118.0" x="132.0" y="120.0"/>
          <y:Fill color="#FFCC00" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="custom" textColor="#000000" visible="true" width="111.70703125" x="3.146484375" y="6.015625">Is content driven?<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
    <node id="n2">
      <data key="d5"><![CDATA[<p>Ok. There is no need to decide between a native app and a mobile app. You should though maybe reconsider your answer again! </p>
<p>Everybody is talking mobile. Smart phones and tablets are changing the way that your users interact with content and online services. 45% of people interact with online content while out and about. That rises to 70% for 16-25s. (UK Sept. 2011). As an online provider you must engage with this emerging market or face losing customers.</p>

]]></data>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="102.0" x="94.25" y="60.0"/>
          <y:Fill color="#FF0000" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="13" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="19.125" modelName="custom" textColor="#000000" visible="true" width="91.0009765625" x="5.49951171875" y="5.4375">We are done.<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
    <node id="n3">
      <data key="d5"><![CDATA[<p class="question">Is your online presence task orientated?</p>]]></data>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="102.0" x="220.5" y="180.0"/>
          <y:Fill color="#FFCC00" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="custom" textColor="#000000" visible="true" width="92.37109375" x="4.814453125" y="6.015625">Task oriented?<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
    <node id="n4">
      <data key="d5"><![CDATA[<h4>You might be better off with just using a mobile optimized version.</h4>
<p>This has the following advantages:</p>
<ul>
<li>Google can crawl the content</li>
<li>Widely accessible via mobile and desktop</li>
<li>Technology is ready and available (responsive or adaptive design)</li>
<li>Costs are fairly low</li>
</ul>
<p>Here are some examples (look at it via mobile device and via desktop browser):</p>
<ul>
<li><a href="http://www.getskeleton.com/" target="_blank">Getskeleton.com</a></li>
<li><a href="http://staffanstorp.se/" target="_blank">Staff Anstorp</a></li>
<li><a href="http://teegallery.com/" target="_blank">tee gallery.com/</a></li>]]></data>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="160.0" x="30.5" y="180.0"/>
          <y:Fill color="#FF0000" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="custom" textColor="#000000" visible="true" width="154.263671875" x="2.8681640625" y="6.015625">Mobile optimized version<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
    <node id="n5">
      <data key="d5"><![CDATA[<p>An app is probably the right thing for you. Lets figure out what kind of app is best for you</p>


<p>There are two types of Apps:</p>
<ul>
<li>web apps that can be accessed via any browser</li>
<li>or native apps, that can be found in the app store</li>
</ul>

<p class="question">My online presence needs to satisfy the following requirements:</p>]]></data>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="171.0" x="15.0" y="240.0"/>
          <y:Fill color="#FFCC00" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="custom" textColor="#000000" visible="true" width="156.14453125" x="7.427734375" y="6.015625">Support different devices<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
    <node id="n6">
      <data key="d5"><![CDATA[<p>So your application is neither content nor task driven. I'm not sure what you want. It would probably be best to start over again or talk to an expert.</p>]]></data>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="193.0" x="216.0" y="240.0"/>
          <y:Fill color="#FF0000" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="13" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="19.125" modelName="custom" textColor="#000000" visible="true" width="186.0380859375" x="3.48095703125" y="5.4375">Not content and task driven<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
    <node id="n7">
      <data key="d5"><![CDATA[<h4>Build a webapp based on HTML5, Javascript and CSS</h4>
<p>If you are not convinced have a look at this very short <a href="http://issuu.com/boagworld/docs/mobilemaze/9" target="_blank">e-book</a>.</p>]]></data>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="89.0" x="21.25" y="300.0"/>
          <y:Fill color="#FFCC00" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="custom" textColor="#000000" visible="true" width="51.40234375" x="18.798828125" y="6.015625">webapp<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
    <node id="n8">
      <data key="d5"><![CDATA[<h4>You should probably go with a native app for either Android, iOS or all of them.</h4>
<p>If you are not convinced have a look at this very short <a href="http://issuu.com/boagworld/docs/mobilemaze/9" target="_blank">e-book</a>.</p>]]></data>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="118.0" x="140.25" y="300.0"/>
          <y:Fill color="#FFCC00" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="custom" textColor="#000000" visible="true" width="67.884765625" x="25.0576171875" y="6.015625">native app<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
    <edge id="e0" source="n0" target="n1">
      <data key="d9"><![CDATA[yes]]></data>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="54.5" sy="15.0" tx="29.5" ty="-15.0">
            <y:Point x="232.75" y="45.0"/>
            <y:Point x="220.5" y="45.0"/>
          </y:Path>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel alignment="center" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="side_slider" preferredPlacement="right" ratio="0.0" textColor="#000000" visible="true" width="24.736328125" x="-26.736328125" y="3.078125">yes</y:EdgeLabel>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
    <edge id="e1" source="n0" target="n2">
      <data key="d9"><![CDATA[no]]></data>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="-54.5" sy="15.0" tx="0.0" ty="-15.0">
            <y:Point x="123.75" y="45.0"/>
            <y:Point x="145.25" y="45.0"/>
          </y:Path>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel alignment="center" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="side_slider" preferredPlacement="right" ratio="0.0" textColor="#000000" visible="true" width="18.947265625" x="-20.947265625" y="3.078125">no</y:EdgeLabel>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
    <edge id="e2" source="n1" target="n4">
      <data key="d9"><![CDATA[Yes]]></data>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="-29.5" sy="15.0" tx="0.0" ty="-15.0">
            <y:Point x="161.5" y="165.0"/>
            <y:Point x="110.5" y="165.0"/>
          </y:Path>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel alignment="center" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="side_slider" preferredPlacement="right" ratio="0.0" textColor="#000000" visible="true" width="24.96484375" x="-26.96484375" y="3.078125">Yes</y:EdgeLabel>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
    <edge id="e3" source="n1" target="n3">
      <data key="d9"><![CDATA[no]]></data>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="29.5" sy="15.0" tx="0.0" ty="-15.0">
            <y:Point x="220.5" y="165.0"/>
            <y:Point x="271.5" y="165.0"/>
          </y:Path>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel alignment="center" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="side_slider" preferredPlacement="right" ratio="0.0" textColor="#000000" visible="true" width="20.318359375" x="-22.318359375" y="3.078125">No</y:EdgeLabel>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
    <edge id="e4" source="n3" target="n5">
      <data key="d9"><![CDATA[yes]]></data>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="-25.5" sy="15.0" tx="0.0" ty="-15.0">
            <y:Point x="246.0" y="225.0"/>
            <y:Point x="100.5" y="225.0"/>
          </y:Path>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel alignment="center" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="side_slider" preferredPlacement="right" ratio="0.0" textColor="#000000" visible="true" width="24.736328125" x="-26.736328125" y="3.078125">yes</y:EdgeLabel>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
    <edge id="e5" source="n3" target="n6">
      <data key="d9"><![CDATA[no]]></data>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="25.5" sy="15.0" tx="0.0" ty="-15.0">
            <y:Point x="297.0" y="225.0"/>
            <y:Point x="312.5" y="225.0"/>
          </y:Path>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel alignment="center" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="side_slider" preferredPlacement="right" ratio="0.0" textColor="#000000" visible="true" width="18.947265625" x="-20.947265625" y="3.078125">no</y:EdgeLabel>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
    <edge id="e6" source="n2" target="n1">
      <data key="d9"><![CDATA[I hit the wrong button. I need a mobile web presence.]]></data>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="0.0" sy="15.0" tx="-29.5" ty="-15.0">
            <y:Point x="145.25" y="105.0"/>
            <y:Point x="161.5" y="105.0"/>
          </y:Path>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel alignment="center" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="side_slider" preferredPlacement="right" ratio="0.0" textColor="#000000" visible="true" width="85.85546875" x="-87.85546875" y="3.078125">I need mobile</y:EdgeLabel>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
    <edge id="e7" source="n5" target="n7">
      <data key="d9"><![CDATA[Run on multiple devices, complete editorial freedom, low budget, no fancy performance intense functionlity]]></data>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="-57.0" sy="15.0" tx="-22.25" ty="-15.0"/>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel alignment="center" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="side_slider" preferredPlacement="right" ratio="0.0" textColor="#000000" visible="true" width="96.232421875" x="-98.232421875" y="6.015625">multiple device</y:EdgeLabel>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
    <edge id="e8" source="n5" target="n8">
      <data key="d9"><![CDATA[Performance is crucial, App needs to be available offline and I want to make use of any feature provided by the device]]></data>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="57.0" sy="15.0" tx="0.0" ty="-15.0">
            <y:Point x="157.5" y="285.0"/>
            <y:Point x="199.25" y="285.0"/>
          </y:Path>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel alignment="center" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="side_slider" preferredPlacement="right" ratio="0.0" textColor="#000000" visible="true" width="98.646484375" x="-100.646484375" y="3.078125">fancy, high perf</y:EdgeLabel>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
    <edge id="e9" source="n5" target="n7">
      <data key="d9"><![CDATA[Short time to market, great deal of flexibility, wide accessibility]]></data>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="0.0" sy="15.0" tx="22.25" ty="-15.0">
            <y:Point x="100.5" y="285.0"/>
            <y:Point x="88.0" y="285.0"/>
          </y:Path>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel alignment="center" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="17.96875" modelName="six_pos" modelPosition="tail" preferredPlacement="anywhere" ratio="0.5" textColor="#000000" visible="true" width="148.73828125" x="-80.619140625" y="17.0">short development time</y:EdgeLabel>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
  </graph>
  <data key="d0">
    <y:Resources/>
  </data>
</graphml>
